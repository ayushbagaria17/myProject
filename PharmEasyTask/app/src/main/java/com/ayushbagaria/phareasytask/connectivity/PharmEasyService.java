package  com.ayushbagaria.phareasytask.connectivity;

import com.squareup.okhttp.ResponseBody;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;

public interface PharmEasyService {

  @GET("/autocomplete?name=b&pageSize=10000000&_=1435404923427")
  Call<ResponseBody> getResponse();


}
