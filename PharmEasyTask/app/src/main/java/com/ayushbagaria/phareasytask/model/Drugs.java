package com.ayushbagaria.phareasytask.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

@Table(name = "Drugs")
public class Drugs extends Model {

    @Expose
    @Column(name = "generics")
    public String generics;

    @Expose
    @Column(name = "type")
    public String type;

    @Expose
    @Column(name = "available")
    public Boolean available;

    @Expose
    @Column(name = "Drugid")
    public int Drugid;

    @Expose
    @Column(name = "name")
    public String name;

    @Expose
    @Column(name = "mfId")
    public String mfId;

    @Expose
    @Column(name = "productsForBrand")
    public String productsForBrand;

    @Expose
    @Column(name = "discountPerc")
    public int discountPerc;

    @Expose
    @Column(name = "uPrice")
    public float uPrice;

    @Expose
    @Column(name = "mrp")
    public float mrp;

    @Expose
    @Column(name = "packSize")
    public String packSize;

    @Expose
    @Column(name = "slug")
    public int slug;

    @Expose
    @Column(name = "imgUrl")
    public String imgUrl;

    @Expose
    @Column(name = "hkpDrugCode")
    public int hkpDrugCode;

    @Expose
    @Column(name = "su")
    public int su;

    @Expose
    @Column(name = "oPrice")
    public long oPrice;

    @Expose
    @Column(name = "packForm")
    public String packForm;

    @Expose
    @Column(name = "label")
    public String label;

    @Expose
    @Column(name = "manufacturer")
    public String manufacturer;

    @Expose
    @Column(name = "form")
    public String form;

    @Expose
    @Column(name = "pForm")
    public String Tablet;

    @Expose
    @Column(name = "uip")
    public int uip;

    @Column(name = "DrugResponse", onDelete = Column.ForeignKeyAction.CASCADE)
    public DrugResponse DrugResponse;

    public Drugs()
    {
        super();
    }

}
