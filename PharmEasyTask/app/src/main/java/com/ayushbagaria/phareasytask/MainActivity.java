package com.ayushbagaria.phareasytask;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.ayushbagaria.phareasytask.connectivity.PharmEasyConnection;
import com.ayushbagaria.phareasytask.connectivity.PharmEasyConnector;
import com.ayushbagaria.phareasytask.listener.ResultCallBack;
import com.ayushbagaria.phareasytask.model.DrugResponse;
import com.ayushbagaria.phareasytask.model.Drugs;

import java.nio.channels.SelectableChannel;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    PharmEasyConnection pharmEasyConnection;
    List<Model> drugResponses;
    private ResultCallBack<DrugResponse> pharmeasyresultcallback = new ResultCallBack<DrugResponse>() {
        @Override
        public void onResultCallBack(DrugResponse drugResponse, Exception e) {
            drugResponse.save();
            for(Drugs drug: drugResponse.getDrugList()){
                drug.DrugResponse = drugResponse;
                drug.save();
            }
            getAllDrugs();
        }

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pharmEasyConnection = new PharmEasyConnection(this);
        pharmEasyConnection.getResponse(pharmeasyresultcallback);
    }

    public void getAllDrugs() {
        drugResponses = new Select()
                .from(Drugs.class)
                .execute();
        Log.d("121", Integer.toString(drugResponses.size()));
    }
}
