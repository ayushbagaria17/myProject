package com.ayushbagaria.phareasytask.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

@Table(name = "Drugs")
public class MyContext extends Model {

    @Expose
    @Column(name ="platformHeader")
    public String platformHeader;

    @Expose
    @Column(name ="discount")
    public Integer integer;

    @Expose
    @Column(name ="emailId")
    public String emailId;

    @Expose
    @Column(name ="canRenderHTMLHeader")
    public String canRenderHTMLHeader;

    @Expose
    @Column(name ="login")
    public boolean login;

    @Expose
    @Column(name ="contact_number")
    public long contact_number;

    @Expose
    @Column(name ="storeCode")
    public long storeCode;

    @Expose
    @Column(name ="city")
    public String city;

    @Expose
    @Column(name ="pinCode")
    public String pinCode;


}
