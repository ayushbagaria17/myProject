package com.ayushbagaria.phareasytask.model;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import java.util.List;

@Table(name = "DrugResponse")
public class DrugResponse extends Model{
    @Expose
    @Column(name = "TEMP_USER_ID")
    public String TEMP_USER_ID;
    @Expose
    @Column(name = "suggestions")
    public int[] suggestions;
    @Expose
    @Column(name = "hasMore")
    public boolean hasMore;
    @Expose
    @Column(name = "errors")
    public String errors;
    @Expose
    @Column(name = "searchTerm")
    public String b;
    @Expose
    @Column(name = "totalRecordCount")
    public Integer totalRecordCount;
    @Expose
    @Column(name = "header")
    public String header;
    @Expose
    @Column(name = "message")
    public String message;
    @Expose
    @Column(name = "status")
    public Integer status;
    @Expose
    @Column(name = "op")
    public String op;
    @Expose
    @Column(name = "context")
    public MyContext context;

    public List<Drugs> result;

    public List<Drugs> getDrugList()
    {
        return getMany(Drugs.class, "DrugResponse");
    }
    public DrugResponse()
    {
        super();
    }

}
