package  com.ayushbagaria.phareasytask.connectivity;

import android.content.Context;
import android.util.Log;

import com.ayushbagaria.phareasytask.common.JsonConverter;
import com.ayushbagaria.phareasytask.listener.ResultCallBack;
import com.ayushbagaria.phareasytask.model.DrugResponse;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.ResponseBody;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class PharmEasyConnection {

  private static final String LOG_TAG = PharmEasyConnection.class.getSimpleName();
  private com.ayushbagaria.phareasytask.connectivity.PharmEasyConnector retroFitConnector;


  public PharmEasyConnection(Context context) {
    retroFitConnector = new PharmEasyConnector();
  }

  public void getResponse(final ResultCallBack<DrugResponse> resultCallBack) {
    PharmEasyService service = retroFitConnector.createService(PharmEasyService.class);
    Call<ResponseBody> request = service.getResponse();
    request.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
        if (response.isSuccess()) {
          String responseString = "";
          try {
            responseString = response.body().string();
          } catch (Exception e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));
          }
          Log.d("121",responseString);
          Log.d("12121","121212");
          DrugResponse drugResponse = new JsonConverter().fromJson(responseString, new TypeToken<DrugResponse>() {
          }.getType());
          resultCallBack.onResultCallBack(drugResponse, null);
        } else {
          resultCallBack.onResultCallBack(null, new Exception());
        }
      }

      @Override
      public void onFailure(Throwable t) {
        t.printStackTrace(System.err);
        resultCallBack.onResultCallBack(null, new Exception());
      }
    });
  }
}